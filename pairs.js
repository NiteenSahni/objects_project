let pairs = (obj) => {
    let pairArray = []
    let i = 0;

    for (let key in obj) {
        pairArray[i] = []
        pairArray[i].push(key, obj[key])
        i = i + 1
    }

    return pairArray
}
module.exports = pairs;
