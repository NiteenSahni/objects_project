let mapObject = (object, cb) => {
    if (typeof (object) == "object") {
        for (let key in object) {
            if (typeof (object[key]) !== "function") {
                object[key] = cb(object[key])
            }
        }

        return object
    }
}

module.exports = mapObject;

