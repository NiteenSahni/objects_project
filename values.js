let values = (object) => {

    let valueArray = []
    if (typeof (object) == "object") {
        for (let key in object) {
            if (typeof (object[`${key}`]) !== "function") {
                valueArray.push(object[`${key}`])
            }
        }
    }
    return valueArray;
}


module.exports = values;