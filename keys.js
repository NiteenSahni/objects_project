let keys = (object) => {
    if (typeof (object) == "object") {
        let array = []
        for (let key in object) {
            array.push(`${key}`)
        }
        return array;
    }
    else{
        return []
    }
}

module.exports = keys;
// console.log(Object.keys(testObject))