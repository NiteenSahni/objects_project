let invert = (object) => {
    let reverseObject = {}
    if (typeof (object) == "object") {
        for (let key in object) {
            reverseObject[object[key]] = key
        }
        return reverseObject
    }
}

module.exports = invert;