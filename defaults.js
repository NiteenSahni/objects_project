

let defaults = (object, defaultProps) => {
    if (typeof (object) == "object") {
        for (let key in defaultProps) {
            if (key in object == false) {
                object[key] = defaultProps[key]
            }
        }
        return object
    }
}
module.exports = defaults;



